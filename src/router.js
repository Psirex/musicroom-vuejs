import VueRouter from 'vue-router'
import Vue from 'vue'
Vue.use(VueRouter)
export var router = new VueRouter()

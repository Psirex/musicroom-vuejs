import {INSERT_ARTIST, INSERT_ARTISTS} from './artistColorsMutationTypes'

const state = {}

const mutations = {
  [INSERT_ARTIST] (state, artist) {
    if (state[artist] === undefined) {
      state[artist] = {
        r: Math.random(),
        g: Math.random(),
        b: Math.random()
      }
    }
  },
  [INSERT_ARTISTS] (state, artists) {
    artists.forEach((artist) => {
      if (state[artist] === undefined) {
        state[artist] = {
          r: Math.floor(Math.random() * 255),
          g: Math.floor(Math.random() * 255),
          b: Math.floor(Math.random() * 255)
        }
      }
    })
    console.log(state)
  }
}

export default { state, mutations }

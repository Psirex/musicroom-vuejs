import {SET_QUERY, SET_USER_SONGS, SET_GLOBAL_SONGS, SET_GLOBAL_SEARCH_TIMER} from './songsMutationTypes'
import {SET_CURRENT_SONG} from '../player/playerMutationTypes'
import {getSongs as getSongsHelper, findSongs} from '../../helpers/vk-helper'
import {getSongs} from './songsGetters'
import {INSERT_ARTISTS} from '../artist-colors/artistColorsMutationTypes'
const setSongs = function setSongs (store, ownerId) {
  getSongsHelper(ownerId).then(function (res) {
    const isEmpty = getSongs(store).length === 0
    console.log(SET_USER_SONGS)
    store.dispatch(SET_USER_SONGS, res)
    const artists = res.map((item) => item.artist.toLowerCase())
    store.dispatch(INSERT_ARTISTS, artists)
    console.log(SET_CURRENT_SONG)
    if (!isEmpty) {
      store.dispatch(SET_CURRENT_SONG, res[0])
    }
  })
}

const setQuery = (store, query) => {
  store.dispatch(SET_QUERY, query)
  const timer = setTimeout(() => {
    console.log('i am action')
    if (!query) {
      store.dispatch(SET_GLOBAL_SONGS, [])
      return
    }
    findSongs(0, query).then((res) => {
      console.log(res)
      store.dispatch(SET_GLOBAL_SONGS, res)
      const artists = res.map((item) => item.artist.toLowerCase())
      store.dispatch(INSERT_ARTISTS, artists)
    })
  }, 700)
  store.dispatch(SET_GLOBAL_SEARCH_TIMER, timer)
}

export {setSongs, setQuery}

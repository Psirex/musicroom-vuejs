const getSongs = (state) => {
  console.log('GET SONGS')
  if (state.songs === undefined) {
    return []
  }
  let res = {
    userSongs: [],
    globalSongs: []
  }
  const filter = state.songs.query.toLowerCase()
  if (!filter) {
    res.userSongs = state.songs.userSongs
  } else {
    res.userSongs = state.songs.userSongs.filter((value) => {
      const title = value.title.toLowerCase()
      const artist = value.artist.toLowerCase()
      return title.indexOf(filter) !== -1 || artist.indexOf(filter) !== -1
    })
    res.globalSongs = state.songs.globalSongs
  }
  return res
  // return state.songs.use.filter((value) => {
  //   return value.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1 ||
  //     value.artist.toLowerCase().indexOf(filter.toLowerCase()) !== -1
  // })
}

const getQuery = (state) => {
  return state.songs.query
}

export {getSongs, getQuery}

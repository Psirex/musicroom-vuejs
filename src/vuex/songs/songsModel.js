import {SET_QUERY, SET_GLOBAL_SONGS, SET_USER_SONGS, SET_GLOBAL_SEARCH_TIMER} from './songsMutationTypes.js'
const state = {
  query: '',
  globalSearchTimer: null,
  all: [],
  userSongs: [],
  globalSongs: []
}

const mutations = {
  [SET_QUERY] (state, query) {
    state.query = query
  },
  [SET_GLOBAL_SEARCH_TIMER] (state, timer) {
    if (state.globalSearchTimer !== null) {
      clearTimeout(state.globalSearchTimer)
    }
    state.globalSearchTimer = timer
  },
  [SET_GLOBAL_SONGS] (state, songs) {
    state.globalSongs = songs
  },
  [SET_USER_SONGS] (state, songs) {
    console.log('SET USER SONGS')
    state.userSongs = songs
  }
}

export default {state, mutations}

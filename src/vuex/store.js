import Vue from 'vue'
import Vuex from 'vuex'
import songs from './songs/songsModel'
import session from './session/sessionModel'
import player from './player/playerModel'
import artistColors from './artist-colors/artistColorsModel'
import rooms from './rooms/roomsModel'
Vue.use(Vuex)
var store = new Vuex.Store({
  modules: {
    songs,
    session,
    player,
    rooms,
    artistColors
  }
})

export default store

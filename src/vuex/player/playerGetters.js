const getPassedTime = (state) => {
  // console.log(state.player.passedTime)
  return state.player.passedTime
}

const getSongUrl = (state) => {
  console.log('get song url')
  console.log(state)
  if (!state.player.currentSong) {
    return 'hello'
  }
  return state.player.currentSong.url
}

const getDuration = (state) => {
  if (!state.player.currentSong) {
    return 0
  }
  return state.player.currentSong.duration
}

const getType = (state) => state.player.type

const getState = (state) => state.player.state

const getCurrentSong = (state) => state.player.currentSong

const getVolumeLevel = (state) => state.player.volumeLevel

export {getPassedTime, getDuration, getSongUrl, getType, getState, getCurrentSong, getVolumeLevel}

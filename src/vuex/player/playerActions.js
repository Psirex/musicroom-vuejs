import {SET_CURRENT_SONG, SET_PASSED_TIME, PLAY, PAUSE, TOGGLE_STATE, SET_VOLUME_LEVEL} from './playerMutationTypes.js'

const setCurrentSong = function setCurrentSong (store, song) {
  console.log('SET_CURRENT_SONG')
  console.log(song)
  store.dispatch(SET_CURRENT_SONG, song)
}

const setPassedTime = function setPassedTime (store, event) {
  store.dispatch(SET_PASSED_TIME, event.target.currentTime)
}

const play = function play (store) {
  store.dispatch(PLAY)
}

const pause = function pause (store) {
  store.dispatch(PAUSE)
}

const toggleState = (store) => {
  store.dispatch(TOGGLE_STATE)
}

const setVolumeLevel = (store, value) => {
  console.log('setVolumeLevel')
  store.dispatch(SET_VOLUME_LEVEL, value)
}

export {setCurrentSong, setPassedTime, play, pause, toggleState, setVolumeLevel}

import {SET_CURRENT_SONG, SET_PASSED_TIME, PLAY, PAUSE, TOGGLE_STATE, SET_VOLUME_LEVEL} from './playerMutationTypes.js'

const state = {
  currentSong: {
    artist: '',
    title: '',
    url: '',
    duration: 0
  },
  volumeLevel: 0.75,
  type: 'SONGS',
  passedTime: 0,
  state: 'STOP' // STOP PLAY PAUSE
}

const mutations = {
  [SET_CURRENT_SONG] (state, song) {
    console.log(state)
    state.currentSong = song
    state.currentSong.url = song.url
    state.passedTime = 0
    state.state = (state.state === 'STOP') ? 'PAUSE' : 'PLAY'
  },
  [SET_PASSED_TIME] (state, time) {
    state.passedTime = time
  },
  [PLAY] (state) {
    state.state = 'PLAY'
  },
  [PAUSE] (state) {
    state.state = 'PAUSE'
  },
  [TOGGLE_STATE] (state) {
    console.log('TOGGLE_STATE')
    if (state.state === 'PLAY') {
      state.state = 'PAUSE'
    } else {
      state.state = 'PLAY'
    }
  },
  [SET_VOLUME_LEVEL] (state, level) {
    state.volumeLevel = level
  }
}

export default {state, mutations}

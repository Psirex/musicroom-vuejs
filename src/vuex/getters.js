const isLogined = (state) => {
  console.log('is logined function')
  console.log(state)
  return state.session.expire !== '' &&
    state.session.mid !== '' &&
    state.session.sid !== '' &&
    state.session.sig !== ''
}

const getSongs = (state) => {
  return state.songs.all
}

const getOwnerId = (state) => {
  return state.session.mid
}

const getPassedTime = (state) => {
  console.log(state.player.passedTime)
  return state.player.passedTime
}

const getSongUrl = (state) => {
  console.log('get song url')
  console.log(state)
  if (!state.player.currentSong) {
    return 'hello'
  }
  return state.player.currentSong.url
}

const getDuration = (state) => {
  if (!state.player.currentSong) {
    return 0
  }
  return state.player.currentSong.duration
}

export {isLogined, getSongs, getOwnerId, getPassedTime, getDuration, getSongUrl}

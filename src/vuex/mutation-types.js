const VK_LOGIN = 'VK_LOGIN'

const SET_QUERY = 'SET_QUERY'
const SET_SONGS = 'SET_SONGS'

// ----------------- PLAYER MUTATIONS ------------------------
const SET_CURRENT_SONG = 'SET_CURRENT_SONG'
const SET_PASSED_TIME = 'SET_PASSED_TIME'
const PLAY = 'PLAY'
const PAUSE = 'PAUSE'
export {VK_LOGIN
        , SET_QUERY
        , SET_SONGS
        , SET_CURRENT_SONG
        , SET_PASSED_TIME
        , PLAY
        , PAUSE}

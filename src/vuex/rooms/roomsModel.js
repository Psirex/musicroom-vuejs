import {SET_ROOMS, SET_CURRENT_ROOM} from './roomsMutationTypes'
const state = {
  all: [],
  currentRoom: null
}

const mutations = {
  [SET_ROOMS] (state, rooms) {
    console.log(SET_ROOMS)
    state.all = rooms
  },
  [SET_CURRENT_ROOM] (state, room) {
    console.log(SET_CURRENT_ROOM)
    state.currentRoom = room
  }
}

export default {state, mutations}

function getRooms (state) {
  console.log('GET_ROOMS GETtter')
  console.log(state.rooms.all)
  return state.rooms.all
}

function getCurrentRoom (state) {
  console.log('GET_CURRENT ROOM')
  return state.rooms.curretnRoom
}

function getCurrentRoomTitle (state) {
  console.log('GET_CURRENT ROOM')
  if (state.rooms.currentRoom) {
    return state.rooms.currentRoom.title
  }
  return ''
}

export {getRooms, getCurrentRoom, getCurrentRoomTitle}

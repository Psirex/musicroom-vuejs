import axios from 'axios'

function getAllRooms () {
  console.log('GET_ALL_ROOMS')
  return axios.get('http://localhost:3000/api/rooms', {withCredentials: true})
}

function getRoom (roomId) {
  console.log('GET ROOM WITH ID: ' + roomId)
  return axios.get('http://localhost:3000/api/rooms/' + roomId,
    {withCredentials: true})
}
export {getAllRooms, getRoom}

import {getAllRooms, getRoom} from './roomsRequests'
import {SET_ROOMS, SET_CURRENT_ROOM} from './roomsMutationTypes'

function setRooms (store) {
  getAllRooms().then(function (res) {
    console.log(SET_ROOMS)
    console.log(res.data.rooms)
    store.dispatch(SET_ROOMS, res.data.rooms)
  })
}

function setRoom (store, room) {
  console.log(store)
  console.log(room)
  getRoom(room).then((res) => {
    console.log(SET_CURRENT_ROOM)
    console.log(res.data)
    store.dispatch(SET_CURRENT_ROOM, res.data)
  })
}

export {setRooms, setRoom}

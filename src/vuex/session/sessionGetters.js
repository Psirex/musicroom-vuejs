const isLogined = (state) => {
  console.log('is logined function')
  console.log(state)
  return state.session.expire !== '' &&
    state.session.mid !== '' &&
    state.session.sid !== '' &&
    state.session.sig !== ''
}

export {
  isLogined
}

import {VK_LOGIN} from './sessionMutationTypes'
import {setCookie} from '../../helpers/cookie-helper'
const state = {
  firstName: '',
  lastName: '',
  mid: '',
  sid: '',
  sig: '',
  expire: '',
  secret: ''
}

const mutations = {
  [VK_LOGIN] (state, loginStatus) {
    console.log(state)
    console.log('FROM VK_LOGIN')
    state.expire = loginStatus.expire
    state.mid = loginStatus.mid
    state.sid = loginStatus.sid
    state.sig = loginStatus.sig
    state.secret = loginStatus.secret
    const cookieProps = {expires: 3600 * 24 * 30}
    setCookie('expire', state.expire, cookieProps)
    setCookie('mid', state.mid, cookieProps)
    setCookie('sid', state.sid, cookieProps)
    setCookie('sig', state.sig, cookieProps)
    setCookie('secret', state.secret, cookieProps)
    console.log(loginStatus)
  }
}

export default {state, mutations}

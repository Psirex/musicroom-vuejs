import axios from 'axios'

function loginOnServer (user) {
  console.log('LOGIN_ON_SERVER')
  return axios.post('http://localhost:3000/api/login',
    user,
    {withCredentials: true}).then((response) => {
      console.log(response)
      return response
    })
}

export {loginOnServer}

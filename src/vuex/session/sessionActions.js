import {login, getLoginStatus as vkGetLoginStatus} from '../../helpers/vk-helper'
import {VK_LOGIN} from './sessionMutationTypes'
import {loginOnServer} from './sessionRequests'
const loginInVK = function loginInVK (store) {
  return login().then(function (res) {
    let result
    switch (res.status) {
      case 'connected':
        store.dispatch(VK_LOGIN, res.session)
        console.log('connected')
        result = {success: true, vkRes: res}
        break
      case 'not_authorized':
        console.log('not_authorized')
        result = {success: false, error: 'Пользователь откланил условия'}
        break
      case 'unknown':
        console.log('unknown')
        result = {success: false, error: 'Пользователь не авторизован'}
        break
    }
    return result
  }).then((res) => {
    if (res.success) {
      const user = {
        firstName: res.vkRes.session.user.first_name,
        lastName: res.vkRes.session.user.last_name,
        id: res.vkRes.session.user.id
      }
      console.log(user)
      return loginOnServer(user)
    } else {
      return res
    }
  })
}

const getLoginStatus = (store) => {
  console.log('GET LOG STATUS')
  return new Promise((resolve, reject) => {
    vkGetLoginStatus().then((res) => {
      console.log(res)
      switch (res.status) {
        case 'connected':
          store.dispatch(VK_LOGIN, res.session)
          resolve({success: true})
          console.log('connected')
          break
        case 'not_authorized':
          resolve({success: false, error: 'Пользователь откланил условия'})
          console.log('not_authorized')
          break
        case 'unknown':
          resolve({success: false, error: 'Пользователь не авторизован'})
          console.log('unknown')
          break
      }
    })
  })
}

export {
  loginInVK, getLoginStatus
}

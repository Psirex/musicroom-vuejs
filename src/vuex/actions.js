import {login, getSongs as getSongsHelper} from '../helpers/vk-helper'
import {VK_LOGIN, SET_SONGS, SET_CURRENT_SONG, SET_PASSED_TIME, PLAY, PAUSE} from './mutation-types'

const loginInVK = function loginInVK (store) {
  return new Promise((resolve, reject) => {
    login().then(function (res) {
      switch (res.status) {
        case 'connected':
          store.dispatch(VK_LOGIN, res.session)
          resolve({success: true})
          console.log('connected')
          break
        case 'not_authorized':
          resolve({success: false, error: 'Пользователь откланил условия'})
          console.log('not_authorized')
          break
        case 'unknown':
          resolve({success: false, error: 'Пользователь не авторизован'})
          console.log('unknown')
          break
      }
    })
  })
}

const getSongs = function getSongs (store, ownerId) {
  getSongsHelper(ownerId).then(function (res) {
    console.log(res)
    store.dispatch(SET_SONGS, res)
    store.dispatch(SET_CURRENT_SONG, res[0])
  })
}

const setCurrentSong = function setCurrentSong (store, song) {
  console.log('SET_CURRENT_SONG')
  store.dispatch(SET_CURRENT_SONG, song)
}

const setPassedTime = function setPassedTime (store, event) {
  console.log(event.target.currentTime)
  store.dispatch(SET_PASSED_TIME, event.target.currentTime)
}

const play = function play (store) {
  store.dispatch(PLAY)
}

const pause = function pause (store) {
  store.dispatch(PAUSE)
}

export {loginInVK, getSongs, setCurrentSong, setPassedTime, play, pause}

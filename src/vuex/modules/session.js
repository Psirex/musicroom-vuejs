import {VK_LOGIN} from '../mutation-types'

const state = {
  mid: '',
  sid: '',
  sig: '',
  expire: '',
  secret: ''
}

const mutations = {
  [VK_LOGIN] (state, loginStatus) {
    console.log(state)
    console.log('FROM VK_LOGIN')
    state.expire = loginStatus.expire
    state.mid = loginStatus.mid
    state.sid = loginStatus.sid
    state.sig = loginStatus.sig
    state.secret = loginStatus.secret
    console.log(loginStatus)
  }
}

export default {state, mutations}

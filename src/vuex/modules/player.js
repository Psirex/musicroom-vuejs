import {SET_CURRENT_SONG, SET_PASSED_TIME, PLAY, PAUSE} from '../mutation-types'

const state = {
  currentSong: null,
  type: 'ROOM',
  passedTime: 0,
  state: 'PAUSE'
}

const mutations = {
  [SET_CURRENT_SONG] (state, song) {
    console.log(state)
    state.currentSong = song
    state.currentSong.url = song.url
  },
  [SET_PASSED_TIME] (state, time) {
    state.passedTime = time
  },
  [PLAY] (state) {
    state.state = 'PLAY'
  },
  [PAUSE] (state) {
    state.state = 'PAUSE'
  }
}

export default {state, mutations}

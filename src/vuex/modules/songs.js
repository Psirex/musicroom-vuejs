import {SET_QUERY, SET_SONGS} from '../mutation-types'
const state = {
  query: '',
  all: []
}

const mutations = {
  [SET_QUERY] (state, query) {
    state.query = query
  },

  [SET_SONGS] (state, songs) {
    state.all = songs
  }
}

export default {state, mutations}

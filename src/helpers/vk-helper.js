const findSongs = (searchType, queryString) => {
  return new Promise((resolve, reject) => {
    window.VK.Api.call('audio.search', {q: queryString, auto_complete: 1, search_own: searchType, count: 100},
      (response) => {
        if (response.response) {
          let res = response.response.slice(1)
          resolve(res)
        } else {
          reject({error: 'error loading data'})
        }
      })
  })
}

const login = () => {
  return new Promise((resolve, reject) => {
    window.VK.Auth.login((res) => {
      console.log(res)
      resolve(res)
    })
  })
}

const getLoginStatus = () => {
  return new Promise((resolve, reject) => {
    window.VK.Auth.getLoginStatus((res) => {
      resolve(res)
    })
  })
}

const getSession = () => {
  return window.VK.Auth.getSession()
}

const getSongs = (ownerId) => {
  return new Promise((resolve, reject) => {
    window.VK.Api.call('audio.get', {owner_id: ownerId, count: 1000},
      (res) => {
        if (res.response) {
          let songs = res.response.slice(1)
          console.log(res.response)
          resolve(songs)
        } else {
          reject({error: 'loading error'})
        }
      })
  })
}

export { findSongs
  , login
  , getLoginStatus
  , getSession
  , getSongs}

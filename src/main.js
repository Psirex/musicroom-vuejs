import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import VueResource from 'vue-resource'
// import VueRouter from 'vue-router'
import Content from './components/mr-content'
import Login from './components/Login'
import SongsSet from './components/songs-set'
import Room from './components/Room'
import {router} from './router'
import RoomsSet from './components/rooms-set'
global.jQuery = require('jquery')
const $ = global.jQuery
window.$ = $
// import Header from './components/header/header-player'
// Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueResource)
Vue.filter('toTime', (value) => {
  let m = Math.floor(value / 60)
  let s = value % 60
  if (s < 10) {
    s = '0' + s
  }
  return `${m}:${s}`
})
Vue.http.options.credentials = true
router.map({
  '/login': {
    component: Login
  },
  '/app': {
    component: App,
    subRoutes: {
      '/songs': {
        component: SongsSet
      },
      '/rooms': {
        component: RoomsSet
      },
      '/rooms/:roomId': {
        name: 'room',
        component: Room
      }
    }
  }
})

router.redirect({
  '/app': '/app/songs',
  '/': '/login'
})

// router.redirect({
//   '*': '/login'
// })

/* eslint-disable no-new */
// new Vue({
//   el: 'body',
//   components: { App }
// })

router.start(Content, '#app')
